# Starter App

## View the image in public/preview to see what the final result should look like

### 1. Create a title header bar with a name of a fake site

### 2. Add a short text box with some place holder text in a shadowed box

### 3. Create a navigation side bar with links (Links don't need to actually work)

### When complete, zip the project into a single file and email it back for review.